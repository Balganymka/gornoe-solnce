module.exports = {
	singleQuote: true,
	semi: false,
	bracketSpacing: false,
	arrowParens: 'avoid',
	endOfLine: 'lf',
	useTabs: true,
	trailingComma: 'es5',
	htmlWhitespaceSensitivity: 'ignore',
}
