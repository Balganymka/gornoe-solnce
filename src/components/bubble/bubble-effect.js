$(document).ready(function() {
	var classes = [
		'.settings',
		'.bubble',
		'.socket-icon',
		'.quality',
		'.docs',
		'.parking',
	]
	animateDiv(classes)
})

function makeNewPosition() {
	// Get viewport dimensions (remove the dimension of the div)
	var h = $('.bubbles').height() - 50
	var w = $('.bubbles').width() - 50

	var nh = Math.floor(Math.random() * h)
	var nw = Math.floor(Math.random() * w)

	return [nh, nw]
}

function animateDiv(myclass) {
	var w = $('.bubbles').width()
	if (w > 768) {
		var coordinates = [
			{
				left: '68%',
				top: '22%',
			},
			{
				left: '84%',
				top: '50%',
			},
			{
				left: '52%',
				top: '80%',
			},
			{
				left: '20%',
				top: '70%',
			},
			{
				left: '10%',
				top: '40%',
			},
			{
				left: '34%',
				top: '10%',
			},
		]
	} else if (w > 480) {
		var coordinates = [
			{
				left: '78%',
				top: '22%',
			},
			{
				left: '84%',
				top: '50%',
			},
			{
				left: '52%',
				top: '80%',
			},
			{
				left: '20%',
				top: '70%',
			},
			{
				left: 0,
				top: '40%',
			},
			{
				left: '20%',
				top: '14%',
			},
		]
	} else {
		var coordinates = [
			{
				left: '68%',
				top: '42%',
			},
			{
				left: '74%',
				top: '60%',
			},
			{
				left: '52%',
				top: '80%',
			},
			{
				left: '13%',
				top: '70%',
			},
			{
				left: 0,
				top: '54%',
			},
			{
				left: '20%',
				top: '32%',
			},
		]
	}

	var n = myclass.length
	for (var i = 0; i < myclass.length; ++i) {
		var newq = makeNewPosition()
		$(myclass[i]).animate(
			{top: coordinates[i].top, left: coordinates[i].left},
			3500
		)
	}
	var cr = myclass[n - 1]
	myclass.pop()
	myclass.unshift(cr)
	animateDiv(myclass)
}
