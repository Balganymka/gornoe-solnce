import request from 'graphql-request'

// const address = 'http://flask.keo.gg:5000/graphql'
const address = 'https://py.gornoesolnce.kz/graphql'

export default class Catalog {
	static async getApartmentInfo(apNumber) {
		const query = `query (
      $apNumber: Int!
    ){
      apartmentByNumber(apNumber: $apNumber) {
				apArea,
				apIsSold,
				apNumber,
				apRoomNumber,
				blockNumber,
				floorNumber
      }
    }`
		let result = request(address, query, {
			apNumber,
		})
		return result
	}

	static async getApartments() {
		const query = `query{
      apartments{
				apArea,
				apIsSold,
				apNumber,
				apRoomNumber,
				blockNumber,
				floorNumber
      }
    }`
		let result = request(address, query)
		return result
	}

	static async submitEmail({
		apNumber,
		clientEmail,
		clientName,
		clientNumber,
		preferredTime,
	}) {
		const query = `mutation (
			$apNumber: Int!,
			$clientEmail: String!,
			$clientName: String!,
			$clientNumber: String!,
			$preferredTime: String!
    ) {
      submitEmail (
        apNumber: $apNumber,
				clientEmail: $clientEmail,
				clientName: $clientName,
				clientNumber: $clientNumber,
				preferredTime: $preferredTime
      )
    }`
		let res = await request(address, query, {
			apNumber,
			clientEmail,
			clientName,
			clientNumber,
			preferredTime,
		})

		if (!res) {
			return
		}
	}

	static async submitOnlyEmail({
		apNumber,
		clientEmail,
		clientName,
		clientNumber,
		preferredTime,
	}) {
		const query = `mutation (
			$apNumber: Int!,
			$clientEmail: String!,
			$clientName: String!,
			$clientNumber: String!,
			$preferredTime: String!
    ) {
      submitEmail (
        apNumber: $apNumber,
				clientEmail: $clientEmail,
				clientName: $clientName,
				clientNumber: $clientNumber,
				preferredTime: $preferredTime
      )
    }`
		let res = await request(address, query, {
			apNumber,
			clientEmail,
			clientName,
			clientNumber,
			preferredTime,
		})

		if (!res) {
			return
		}
	}
}
