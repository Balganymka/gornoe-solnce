import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/components/Main'

Vue.use(Router)

const router = new Router({
	routes: [
		{
			path: '/',
			name: 'Main',
			component: Main,
		},
		{
			path: '/about',
			name: 'About',
			component: Main,
		},
		{
			path: '/features',
			name: 'Features',
			component: Main,
		},
		{
			path: '/layouts',
			name: 'Layouts',
			component: Main,
		},
		{
			path: '/callback',
			name: 'Callback',
			component: Main,
		},
		{
			path: '/flats',
			name: 'Flats',
			component: Main,
		},
		{
			path: '/mapp',
			name: 'Map',
			component: Main,
		},
	],
})

export default router
