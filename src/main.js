// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import AOS from 'aos'
import 'aos/dist/aos.css'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/dist/css/swiper.css'
import vueSmoothScroll from 'vue2-smooth-scroll'
import Vuetify from 'vuetify'

Vue.config.productionTip = false

Vue.use(Vuetify)
Vue.use(VueAwesomeSwiper)
Vue.use(vueSmoothScroll)

/* eslint-disable no-new */
new Vue({
	el: '#app',
	router,
	created() {
		AOS.init({
			once: true,
		});
	},
	components: {App},
	template: '<App/>',
})
